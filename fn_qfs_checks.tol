// -*- c++ -*-

//////////////////////////////////////////////////////////////////////////////
Set QFS.CheckScenarioDrivers.NotFinite(NameBlock qfs) 
//////////////////////////////////////////////////////////////////////////////
{
  WriteLn("[QFS.CheckScenarioDrivers.NotFinite] Checking...");
  Set errDrivers = SetConcat(EvalSet(qfs::_.scenario.drivers, Set (Text sceD) {
    // -----------------------------------------------------------------------
    // Se ha de testear en el fechado de la serie
    //! se toma el fechado de previsión del primer submodelo
    Set dating1 = @TimeSet(Dating(qfs::_.submodels.data[1][7]));
    Set dating2 = @TimeSet(Dating(qfs::_.drivers.data[sceD]));
    // Fecha de inicio (su intervalo ha de contener al inicio del modelo)
    Date begin2 = Succ(Succ(qfs::_.begin, C, 1), $dating2, -1);
    // Fecha de fin (su intervalo ha de contener al final del modelo)
    Date end2 = DateFloor(Succ(Succ(qfs::_.end, $dating1, 1), C, -1), $dating2);    
    // -----------------------------------------------------------------------
    Serie dta = SubSer(qfs::_.drivers.data[sceD], begin2, end2);
    Real notfinite = Case(
      Last(dta)<end2, 1,
      First(dta)>begin2, 1,
      True, SumS(!IsFinite(dta)));
    If(notfinite,  [[ PutName(sceD, sceD) ]], Empty)    
  }));
  Set check = If(Card(errDrivers), {
    Set ed_names = EvalSet(errDrivers, Name);
    Real ed_number = Card(ed_names);
    Text ed_list = TextJoinWith(ed_names, ", ");    
    Text ed_brief = "Found "<<ed_number<<" not finite drivers: "
      <<If(TextLength(ed_list)>100, TextSub(ed_list, 1, 97)<<"...", ed_list);
    Set { [[ 
      Text Function = "QFS.CheckScenarioDrivers.NotFinite";
      Text Brief = ed_brief;
      Set NotFiniteDrivers = { [[ Real Number = ed_number; Set Names = ed_names, Text Brief = ed_brief ]] }
    ]] }
  }, Empty);
  WriteLn("[QFS.CheckScenarioDrivers.NotFinite] Checked!");
  check
};

//////////////////////////////////////////////////////////////////////////////
Set QFS.CheckScenarioInputs.NotFinite(NameBlock qfs, Set submodels) 
//////////////////////////////////////////////////////////////////////////////
{
  WriteLn("[QFS.CheckScenarioInputs.NotFinite] Checking...");
  Set scenario.inputs = Unique(SetConcat(EvalSet(qfs::_.scenario.drivers, 
    Set (Text sceD) {
    qfs::_.drivers.inputs[sceD]
  })));
  Real If(Card(submodels), {
    Set used_inputs = Unique(SetConcat(EvalSet(submodels, 
      Set (Text submodel) { qfs::_.submodels.effects.input[submodel] })));
    Set scenario.inputs := scenario.inputs * used_inputs;
  1});
  Set errInputs = SetConcat(EvalSet(scenario.inputs, Set (Text sceI) {
    Serie dta = SubSer(qfs::GetInput(sceI), qfs::_.begin, qfs::_.end);
    Real notfinite = Case(
      Last(dta)<qfs::_.end, 1,
      First(dta)>qfs::_.begin, 1,
      True, SumS(!IsFinite(dta)));
    If(notfinite,  [[ PutName(sceI, sceI) ]], Empty)    
  }));
  Set check = If(Card(errInputs), {
    Set ei_names = EvalSet(errInputs, Name);
    Set suspDrivers = Sort(Select(EvalSet(qfs::_.scenario.drivers, 
      Real (Text sceD) {
      Real deps = Card(qfs::_.drivers.inputs[sceD]*ei_names);
      PutName(sceD, deps)
    }), Real (Real i) {i}), Real (Real a, Real b) { Compare(b,a) });
    Real ei_number = Card(ei_names);
    Text ei_list = TextJoinWith(ei_names, ", ");    
    Text ei_brief = "Found "<<ei_number<<" not finite inputs: "
      <<If(TextLength(ei_list)>100, TextSub(ei_list, 1, 97)<<"...", ei_list);
    Real sd_number = Card(suspDrivers);
    Set sd_names = EvalSet(suspDrivers, Name);    
    Text sd_list = TextJoinWith(sd_names, ", ");
    Text sd_brief = "Found "<<sd_number<<" suspect drivers: "
      <<If(TextLength(sd_list)>100, TextSub(sd_list, 1, 97)<<"...", sd_list);
    Set { [[ 
      Text Function = "CheckScenarioInputs.NotFinite";
      Text Brief = ei_brief<<"\n"<<sd_brief;
      Set NotFiniteInputs = { [[ Real Number = ei_number; Set Names = ei_names, Text Brief = ei_brief ]] };
      Set SuspectDrivers = { [[ Real Number = sd_number; Set Names = sd_names, Text Brief = sd_brief ]] }
    ]] }
  }, Empty);
  WriteLn("[QFS.CheckScenarioInputs.NotFinite] Checked!");
  check
};